﻿using MedShop.DTOs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MedshopClientWPF
{
    public class RequestController
    {
        public static readonly int ConnectionFailsBy = 5;
        public static readonly string ErrorResultStr = "Err";
        public static readonly string OkResultStr = "OK";

        private int _requestFailedCount;
        public bool IsConnected { get; private set; }

        private DispatcherTimer _connectionCheckTimer;
        public delegate void ConnectionStatusChangedHandler(object sender, EventArgs e);
        public event ConnectionStatusChangedHandler OnConnectionStatusChanged;

        public async Task<string> MakeAsyncRequestAsync(string url, HttpMethod verb, string content = "")
        {
            string verbStr;
            switch (verb)
            {
                case HttpMethod.Post:
                    verbStr = WebRequestMethods.Http.Post;
                    break;
                case HttpMethod.Delete:
                    verbStr = @"DELETE";
                    break;
                case HttpMethod.Get:
                    verbStr = WebRequestMethods.Http.Get;
                    break;
                default:
                    verbStr = "-";
                    break;
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = @"application/json; charset=UTF-8";
            request.Method = verbStr;
            request.Timeout = 4000;
            request.Proxy = null;

            if (verb == HttpMethod.Post)
            {
                request.ContentLength = 0;
                request.Accept = "application/json";
                try
                {
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        streamWriter.Write(content);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }
                catch
                {
                    _requestFailedCount += 1;
                    return ErrorResultStr;
                }
            }

            WebResponse response = null;

            try
            {
                response = await request.GetResponseAsync();
            }
            catch { }
            if (response == null)
            {
                _requestFailedCount += 1;
                return ErrorResultStr;
            }

            return ReadStreamFromResponse(response);
        }

        private string ReadStreamFromResponse(WebResponse response)
        {
            var sc = ((HttpWebResponse)response).StatusCode;
            if ((sc != HttpStatusCode.OK) && (sc != HttpStatusCode.Created))
            {
                _requestFailedCount += 1;
                response.Close();
                return ErrorResultStr;
            }

            using (Stream responseStream = response.GetResponseStream())
            using (StreamReader sr = new StreamReader(responseStream))
            {
                string strContent = sr.ReadToEnd();
                return strContent;
            }
        }

        public RequestController()
        {
            _requestFailedCount = 0;
            _connectionCheckTimer = new DispatcherTimer(DispatcherPriority.Background);
            _connectionCheckTimer.Tick += new EventHandler(ConnectionCheckTimer_Tick);
            _connectionCheckTimer.Interval = new TimeSpan(0, 0, 0, 0, 3000);
            _connectionCheckTimer.Start();
        }

        private bool _connectionCheckTimerIsRunning = false;
        async void ConnectionCheckTimer_Tick(object sender, EventArgs e)
        {
            if (_connectionCheckTimerIsRunning) return;
            _connectionCheckTimerIsRunning = true;

            bool isConnectedOldValue = IsConnected;

            if (_requestFailedCount == ConnectionFailsBy)
            {
                _requestFailedCount = 0;
                IsConnected = false;
            }

            if (!IsConnected)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.bing.com");
                request.Method = WebRequestMethods.Http.Get;
                request.Timeout = 3000;

                WebResponse response = null;
                try
                {
                    response = await request.GetResponseAsync();
                }
                catch (Exception) { }

                if (response != null && ((HttpWebResponse)response).StatusCode == HttpStatusCode.OK)
                {
                    response.Close();
                    IsConnected = true;
                }
            }

            if (IsConnected != isConnectedOldValue)
            {
                var eArgs = new EventArgs();
                OnConnectionStatusChanged(new object(), eArgs);
            }
            _connectionCheckTimerIsRunning = false;
        }
    }

    public class MedShopRequestController : RequestController
    {
        public async Task<List<DtoOrder>> GetAllOrders()
        {
            var orderList = new List<DtoOrder>();

            string data = "";
            for (int i = 0; i < ConnectionFailsBy; i++)
            {
                data = await MakeAsyncRequestAsync(RequestStrings.GetAllOrdersOrAddOrder(), HttpMethod.Get);
                if (data != ErrorResultStr) break;
            }

            orderList = JsonConvert.DeserializeObject<List<DtoOrder>>(data);
            return orderList;
        }

        public async Task<bool> DeleteOrderById(int orderId, int prodId)
        {
            string data = "";
            string url = RequestStrings.AddDeleteOrdersDetails(orderId, prodId);

            for (int i = 0; i < ConnectionFailsBy; i++)
            {
                data = await MakeAsyncRequestAsync(url, HttpMethod.Delete);
                if (data != ErrorResultStr) break;
            }

            if (data == ErrorResultStr) return false;
            return true;
        }

        public async Task<DtoOrder> CreateOrder()
        {
            string data = "";
            for (int i = 0; i < ConnectionFailsBy; i++)
            {
                data = await MakeAsyncRequestAsync(RequestStrings.GetAllOrdersOrAddOrder(), HttpMethod.Post);
                if (data != ErrorResultStr) break;
            }

            return JsonConvert.DeserializeObject<DtoOrder>(data);
        }

        public async Task<DtoOrder> GetOrderById(int id)
        {
            string data = "";
            for (int i = 0; i < ConnectionFailsBy; i++)
            {
                data = await MakeAsyncRequestAsync(RequestStrings.GetOrder(id), HttpMethod.Get);
                if (data != ErrorResultStr) break;
            }

            return JsonConvert.DeserializeObject<DtoOrder>(data);
        }

        public async Task<DtoOrder> ChangeOrderStatusById(int id, EnumOrderStatus stat)
        {
            string data = "";
            string url = "";
            switch (stat)
            {
                case EnumOrderStatus.Closed:
                    url = RequestStrings.ChangeOrderStatClosed(id);
                    break;
                case EnumOrderStatus.Delivered:
                    url = RequestStrings.ChangeOrderStatDelivered(id);
                    break;
                case EnumOrderStatus.Payed:
                    url = RequestStrings.ChangeOrderStatPayed(id);
                    break;
                default:
                    return null;
            }

            for (int i = 0; i < ConnectionFailsBy; i++)
            {
                data = await MakeAsyncRequestAsync(url, HttpMethod.Post);
                if (data != ErrorResultStr) break;
            }

            return JsonConvert.DeserializeObject<DtoOrder>(data);
        }

        public async Task<List<DtoOrderDetail>> GetDetailsForOrderById(int id)
        {
            string data = "";
            string url = RequestStrings.GetOrdersDetails(id);

            for (int i = 0; i < ConnectionFailsBy; i++)
            {
                data = await MakeAsyncRequestAsync(url, HttpMethod.Get);
                if (data != ErrorResultStr) break;
            }

            return JsonConvert.DeserializeObject<List<DtoOrderDetail>>(data);
        }

        public async Task<DtoOrderDetail> AddDetailForOrderById(int orderId, int prodId)
        {
            string data = "";
            string url = RequestStrings.AddDeleteOrdersDetails(orderId, prodId);

            for (int i = 0; i < ConnectionFailsBy; i++)
            {
                data = await MakeAsyncRequestAsync(url, HttpMethod.Post);
                if (data != ErrorResultStr) break;
            }

            return JsonConvert.DeserializeObject<DtoOrderDetail>(data);
        }

        public async Task<bool> DeleteDetailForOrderById(int orderId, int prodId)
        {
            string data = "";
            string url = RequestStrings.AddDeleteOrdersDetails(orderId, prodId);

            for (int i = 0; i < ConnectionFailsBy; i++)
            {
                data = await MakeAsyncRequestAsync(url, HttpMethod.Delete);
                if (data != ErrorResultStr) break;
            }

            if (data == ErrorResultStr) return false;
            return true;
        }
    }

    public enum HttpMethod
    {
        Get, Post, Delete
    }
}
