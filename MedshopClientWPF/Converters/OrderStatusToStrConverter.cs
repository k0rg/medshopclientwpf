﻿using System;
using System.Windows.Data;
using MedShop.DTOs;

namespace MedshopClientWPF.Converters
{
    public class OrderStatusToStrConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (!(value is EnumOrderStatus)) return "-";
            EnumOrderStatus val = (EnumOrderStatus)value;
            switch (val)
            {
                case EnumOrderStatus.Closed:
                    return "Заказ закрыт";
                case EnumOrderStatus.Created:
                    return "Новый заказ";
                case EnumOrderStatus.Delivered:
                    return "Заказ доставлен";
                case EnumOrderStatus.Payed:
                    return "Заказ оплачен";
                default:
                    return "-";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }



}
