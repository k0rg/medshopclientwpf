﻿using System;
using System.Windows.Data;

namespace MedshopClientWPF.Converters
{

    public class DateTimeOffsetToStrConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value == null) { return "не указана"; }
            if (!(value is DateTimeOffset)) return "-";
            DateTimeOffset val = (DateTimeOffset)value;
            return val.LocalDateTime.ToString(culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
