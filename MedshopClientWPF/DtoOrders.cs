﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MedShop.DTOs
{
#pragma warning disable 1591

    [JsonConverter(typeof(StringEnumConverter))]
    public enum EnumOrderStatus
    {
        Created = 0,
        Payed = 1,
        Delivered = 2,
        Closed = 3
    }

    public class DtoOrder
    {
        public int Id { get; set; }
        public EnumOrderStatus OrderStatus { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset? DatePayed { get; set; }
        public DateTimeOffset? DateDelivered { get; set; }
        public DateTimeOffset? DateClosed { get; set; }
        public int CountDetails { get; set; }
    }

    public class DtoOrderDetail
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
    }

#pragma warning restore 1591
}
