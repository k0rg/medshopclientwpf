﻿namespace MedshopClientWPF
{
    public static class RequestStrings
    {
        private static readonly string Domain = "http://medshop.azurewebsites.net/api";
        private static readonly string Controller = "Orders";

        public static string GetOrdersDetails(int orderId) { return $"{Domain}/{Controller}/{orderId}/Details"; }
        public static string AddDeleteOrdersDetails(int orderId, int productId) { return $"{Domain}/{Controller}/{orderId}/Details/{productId}"; }
        public static string GetAllOrdersOrAddOrder() { return $"{Domain}/{Controller}"; }
        public static string ChangeOrderStatPayed(int orderId) { return $"{Domain}/{Controller}/{orderId}/Pay"; }
        public static string ChangeOrderStatDelivered(int orderId) { return $"{Domain}/{Controller}/{orderId}/Deliver"; }
        public static string ChangeOrderStatClosed(int orderId) { return $"{Domain}/{Controller}/{orderId}/Close"; }
        public static string GetOrder(int orderId) { return $"{Domain}/{Controller}/{orderId}"; }
    }
}
