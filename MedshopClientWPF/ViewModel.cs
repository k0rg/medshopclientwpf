﻿using MedShop.DTOs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MedshopClientWPF
{
    public class MedshopViewModel : ViewModelBase
    {
        private MedShopRequestController ReqController;

        public ObservableCollection<RequestsItem> RequestsItemsCollectiom;

        private void RequestsItemsCollectiom_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add && !ExecuteRequestIsRunning)
                ExecuteRequestLoop();
        }

        private bool _isInternetConnected;

        public bool isInternetConnected
        {
            get { return _isInternetConnected; }
            set
            {
                _isInternetConnected = value; OnPropertyChanged();

                if (_isInternetConnected)
                    RequestsItemsCollectiom.Add(new RequestsItem(RequestsType.UPDATE_ORDERS, 0, 0));
            }
        }

        private OrderModel _SelectedOrderVM;

        public OrderModel SelectedOrderVM
        {
            get { return _SelectedOrderVM; }
            set
            {
                _SelectedOrderVM = value;
                OnPropertyChanged();
                OnPropertyChanged("NextStatusStr");
                if (value != null)
                {
                    RequestsItemsCollectiom.Add(new RequestsItem(RequestsType.UPDATE_DETAILS, 0, 0));
                }
            }
        }

        public string NextStatusStr
        {
            get
            {
                string str = "-";

                if (SelectedOrderVM != null)
                {
                    if (SelectedOrderVM.OrderStatus == EnumOrderStatus.Created) return "Отметить как: Оплачено";
                    else if (SelectedOrderVM.OrderStatus == EnumOrderStatus.Payed) return "Отметить как: Доставлено";
                    else if (SelectedOrderVM.OrderStatus == EnumOrderStatus.Delivered) return "Отметить как: Закрыто";
                }

                return str;
            }
        }

        private DtoOrderDetail _SelectedDetail;

        public DtoOrderDetail SelectedDetail
        {
            get { return _SelectedDetail; }
            set { _SelectedDetail = value; OnPropertyChanged(); OnPropertyChanged("SelectedDetailStr"); }
        }

        public string SelectedDetailStr
        {
            get
            {
                if (SelectedDetail == null) return "-";
                else return SelectedDetail.ProductName;
            }
        }

        private ObservableCollection<OrderModel> _OrdersList;

        public ObservableCollection<OrderModel> OrdersList
        {
            get { return _OrdersList; }
            set
            {
                _OrdersList = value; OnPropertyChanged();
                OrderCollectionToBind = CollectionViewSource.GetDefaultView(_OrdersList);
            }
        }

        private ICollectionView _OrderCollectionToBind;

        public ICollectionView OrderCollectionToBind
        {
            get { return _OrderCollectionToBind; }
            set { _OrderCollectionToBind = value; }
        }

        public MedshopViewModel()
        {
            RequestsItemsCollectiom = new ObservableCollection<RequestsItem>();
            RequestsItemsCollectiom.CollectionChanged += RequestsItemsCollectiom_CollectionChanged;
            ReqController = new MedShopRequestController();
            ReqController.OnConnectionStatusChanged += ReqController_OnConnectionStatusChanged;
            OrdersList = new ObservableCollection<OrderModel>();
        }

        private void ReqController_OnConnectionStatusChanged(object sender, EventArgs e)
        {
            isInternetConnected = ReqController.IsConnected;
        }

        public async Task UpdateOrderList()
        {
            //OrderItemVM
            var NewOrdersList = new List<DtoOrder>();

            try
            {
                var newOrderList = await ReqController.GetAllOrders();
                if (newOrderList != null) NewOrdersList = newOrderList;
            }
            catch { }

            OrdersList.Clear();
            foreach (var it in NewOrdersList) OrdersList.Add(new OrderModel(it));
        }

        public async Task AddOrderToList()
        {
            DtoOrder newOrder = null;
            try
            {
                newOrder = await ReqController.CreateOrder();
            }
            catch { }

            if (newOrder != null)
            {
                OrdersList.Add(new OrderModel(newOrder));
            }
        }

        public async Task UpdateDetailsList()
        {
            List<DtoOrderDetail> DetList;
            int ID = SelectedOrderVM.Id;
            try
            {
                DetList = await ReqController.GetDetailsForOrderById(ID);
            }
            catch { DetList = new List<DtoOrderDetail>(); }

            if (DetList != null && SelectedOrderVM.Id == ID)
            {
                SelectedOrderVM.OrderDetailList = new ObservableCollection<DtoOrderDetail>(DetList);
            }
        }

        public async Task ChangeOrderStatus(int orderID, EnumOrderStatus status)
        {
            DtoOrder Order = null;
            try
            {
                Order = await ReqController.ChangeOrderStatusById(orderID, status);
            }
            catch { }

            if (Order != null)
            {
                //if(SelectedOrderVM.Id == orderID)
                //{
                //    SelectedOrderVM = new OrderModel(Order);
                //}

                if (OrdersList.Any(o => o.Id == orderID))
                {
                    var ord = OrdersList.First(o => o.Id == orderID);
                    int ind = OrdersList.IndexOf(ord);
                    OrdersList[ind] = new OrderModel(Order);
                    SelectedOrderVM = OrdersList[ind];
                }
            }
        }

        public async Task AddDetailToOrder(int orderID, int prodID)
        {
            DtoOrderDetail Det = null;
            try
            {
                Det = await ReqController.AddDetailForOrderById(orderID, prodID);
            }
            catch { }

            if (Det != null)
            {
                if (OrdersList.Any(o => o.Id == orderID))
                {
                    var ord = OrdersList.First(o => o.Id == orderID);
                    if (ord.OrderDetailList.Any(o => o.ProductId == prodID))
                    {
                        int ind = ord.OrderDetailList.IndexOf(ord.OrderDetailList.First(o => o.ProductId == prodID));
                        ord.OrderDetailList[ind] = Det;
                        SelectedDetail = ord.OrderDetailList[ind];
                    }
                    else { ord.OrderDetailList.Add(Det); ord.CountDetails += 1; }
                }
            }
        }

        public async Task DeleteDetailInOrder(int orderID, int prodID)
        {
            bool result = false;
            try
            {
                result = await ReqController.DeleteDetailForOrderById(orderID, prodID);
            }
            catch { }

            if (result)
            {
                var ord = OrdersList.First(o => o.Id == orderID);
                if (ord.OrderDetailList.Any(o => o.ProductId == prodID))
                {
                    int ind = ord.OrderDetailList.IndexOf(ord.OrderDetailList.First(o => o.ProductId == prodID));
                    if (ord.OrderDetailList[ind].Quantity > 1)
                    {
                        ord.OrderDetailList[ind].Quantity -= 1;

                        var NewDetail = new DtoOrderDetail()
                        {
                            ProductId = ord.OrderDetailList[ind].ProductId,
                            ProductName = ord.OrderDetailList[ind].ProductName,
                            Quantity = ord.OrderDetailList[ind].Quantity
                        };

                        ord.OrderDetailList[ind] = NewDetail;
                        SelectedDetail = ord.OrderDetailList[ind];
                    }
                    else if (ord.OrderDetailList[ind].Quantity == 1) { ord.OrderDetailList.RemoveAt(ind); ord.CountDetails -= 1; }
                }
            }
        }

        private bool ExecuteRequestIsRunning;

        private async Task ExecuteRequestLoop()
        {
            ExecuteRequestIsRunning = true;
            while (RequestsItemsCollectiom.Count > 0)
            {
                var item = RequestsItemsCollectiom[0];
                await ExecuteRequest(item);
                RequestsItemsCollectiom.Remove(item);
            }
            ExecuteRequestIsRunning = false;
        }

        private async Task ExecuteRequest(RequestsItem item)
        {
            switch (item.type)
            {
                case RequestsType.UPDATE_ORDERS:
                    await UpdateOrderList();
                    break;

                case RequestsType.ADD_ORDER:
                    await AddOrderToList();
                    break;

                case RequestsType.DELETE_ORDER:
                    break;

                case RequestsType.CHANGE_ORDER_STAT:
                    await ChangeOrderStatus(item.arg1, (EnumOrderStatus)item.arg2);
                    break;

                case RequestsType.UPDATE_DETAILS:
                    await UpdateDetailsList();
                    break;

                case RequestsType.ADD_DETAIL:
                    await AddDetailToOrder(item.arg1, item.arg2);
                    break;

                case RequestsType.DELETE_DETAIL:
                    await DeleteDetailInOrder(item.arg1, item.arg2);
                    break;

                default:
                    break;
            }
        }

        public void TrySetNextStatus()
        {
            if (SelectedOrderVM == null || SelectedOrderVM.OrderStatus == EnumOrderStatus.Closed) return;
            RequestsItemsCollectiom.Add(new RequestsItem(RequestsType.CHANGE_ORDER_STAT, SelectedOrderVM.Id, ((int)SelectedOrderVM.OrderStatus) + 1));
        }

        public void TryAddProdByID(int ID)
        {
            if (SelectedOrderVM == null) return;
            int ind = SelectedOrderVM.Id;
            RequestsItemsCollectiom.Add(new RequestsItem(RequestsType.ADD_DETAIL, ind, ID));
            //AddDetailToOrder(int orderID, int prodID)
        }

        public void TryDeleteProdByID(int ID)
        {
            if (SelectedOrderVM == null || !SelectedOrderVM.OrderDetailList.Any(o => o.ProductId == ID)) return;
            int ind = SelectedOrderVM.Id;
            RequestsItemsCollectiom.Add(new RequestsItem(RequestsType.DELETE_DETAIL, ind, ID));
            //AddDetailToOrder(int orderID, int prodID)
        }
    }

    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum RequestsType
    {
        UPDATE_ORDERS,
        ADD_ORDER,
        DELETE_ORDER,

        CHANGE_ORDER_STAT,

        UPDATE_DETAILS,
        ADD_DETAIL,
        DELETE_DETAIL
    }

    public class RequestsItem
    {
        public RequestsType type;
        public int arg1;
        public int arg2;
        private object obj;

        public RequestsItem(RequestsType t, int Arg1, int Arg2)
        {
            type = t;
            arg1 = Arg1;
            arg2 = Arg2;
        }
    }
}