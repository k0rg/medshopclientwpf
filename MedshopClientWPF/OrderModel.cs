﻿using MedShop.DTOs;
using System;
using System.Collections.ObjectModel;

namespace MedshopClientWPF
{
    public class OrderModel : ViewModelBase
    {
        public int Id { get; set; }

        EnumOrderStatus _orderStatus;
        public EnumOrderStatus OrderStatus { get { return _orderStatus; } set { _orderStatus = value; OnPropertyChanged(); } }

        DateTimeOffset _dateCreated;
        public DateTimeOffset DateCreated { get { return _dateCreated; } set { _dateCreated = value; OnPropertyChanged(); } }

        DateTimeOffset? _datePayed;
        public DateTimeOffset? DatePayed { get { return _datePayed; } set { _datePayed = value; OnPropertyChanged(); } }

        DateTimeOffset? _dateDelivered;
        public DateTimeOffset? DateDelivered { get { return _dateDelivered; } set { _dateDelivered = value; OnPropertyChanged(); } }

        DateTimeOffset? _dateClosed;
        public DateTimeOffset? DateClosed { get { return _dateClosed; } set { _dateClosed = value; OnPropertyChanged(); } }

        int _countDetails;
        public int CountDetails { get { return _countDetails; } set { _countDetails = value; OnPropertyChanged(); } }

        bool _isChanging;
        public bool IsChanging { get { return _isChanging; } set { _isChanging = value; OnPropertyChanged(); } }

        ObservableCollection<DtoOrderDetail> _orderDetailList;
        public ObservableCollection<DtoOrderDetail> OrderDetailList { get { return _orderDetailList; } set { _orderDetailList = value; OnPropertyChanged(); } }

        public OrderModel(DtoOrder order)
        {
            OrderDetailList = new ObservableCollection<DtoOrderDetail>();
            IsChanging = false;
            Id = order.Id;
            OrderStatus = order.OrderStatus;
            DateCreated = order.DateCreated;
            DatePayed = order.DatePayed;
            DateDelivered = order.DateDelivered;
            DateClosed = order.DateClosed;
            CountDetails = order.CountDetails;
        }
    }
}
