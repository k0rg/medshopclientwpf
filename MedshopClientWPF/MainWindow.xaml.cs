﻿using MedShop.DTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MedshopClientWPF
{

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MedshopViewModel ViewModel;

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new MedshopViewModel();
            DataContext = ViewModel;
        }

        private void AddOrdBtn_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RequestsItemsCollectiom.Add(new RequestsItem(RequestsType.ADD_ORDER, 0, 0));
        }

        private void UpdOrdBtn_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RequestsItemsCollectiom.Add(new RequestsItem(RequestsType.UPDATE_ORDERS, 0, 0));
        }

        //private void DelOrdBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    if (ViewModel.SelectedOrder == null || ViewModel.SelectedOrder.isChanging == true) return;
        //    ViewModel.SelectedOrder.isChanging = true;
        //    ViewModel.SelectedOrder.OnPropertyChanged("isChanging");
        //    ViewModel.RequestsItemsCollectiom.Add(new RequestsItem(RequestsType.DELETE_ORDER, ViewModel.SelectedOrder.Id, 0));
        //}

        private void SortIdBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.OrderCollectionToBind != null && !ViewModel.OrderCollectionToBind.IsEmpty)
            {
                ViewModel.OrderCollectionToBind.SortDescriptions.Clear();
                ViewModel.OrderCollectionToBind.SortDescriptions.Add(new System.ComponentModel.SortDescription("Id", ListSortDirection.Ascending));
            }
        }

        private void SortStBtn_Click(object sender, RoutedEventArgs e)
        {
            if(ViewModel.OrderCollectionToBind != null && !ViewModel.OrderCollectionToBind.IsEmpty)
            {
                ViewModel.OrderCollectionToBind.SortDescriptions.Clear();
                ViewModel.OrderCollectionToBind.SortDescriptions.Add(new System.ComponentModel.SortDescription("OrderStatus", ListSortDirection.Ascending));
            }
        }

        private void ChangeStatus_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedOrderVM == null) return;
            ViewModel.TrySetNextStatus();
        }

        private void AddProdBtn_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedOrderVM == null || ViewModel.SelectedOrderVM.OrderStatus != EnumOrderStatus.Created)
                return;


            (sender as Button).ContextMenu.PlacementTarget = (sender as Button);
            (sender as Button).ContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
            (sender as Button).ContextMenu.IsOpen = true;
            (sender as Button).ContextMenu.StaysOpen = true; 
        }

        private void Prod1_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.TryAddProdByID(1);
        }

        private void Prod2_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.TryAddProdByID(2);
        }

        private void Prod3_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.TryAddProdByID(3);
        }

        private void Prod4_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.TryAddProdByID(4);
        }

        private void DetPlus1_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedOrderVM == null || ViewModel.SelectedOrderVM.OrderStatus != EnumOrderStatus.Created)
                return;

            if (ViewModel.SelectedDetail != null) ViewModel.TryAddProdByID(ViewModel.SelectedDetail.ProductId);
        }

        private void DetMinus1_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedOrderVM == null || ViewModel.SelectedOrderVM.OrderStatus != EnumOrderStatus.Created)
                return;

            if (ViewModel.SelectedDetail != null) ViewModel.TryDeleteProdByID(ViewModel.SelectedDetail.ProductId);
        }
    }
}
